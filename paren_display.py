"""paren_display.py
"""

from pybol import *

paren_pat_6 = cat(exact('(6.'), set('mode', skip(1)), set('ending', skip(1)),
                  exact(')'))

def paren_display_6(text, start, end):
    dic = match(text, paren_pat_6, start, end)
    if dic == None:
        return None

    mode = dic['mode']
    if mode not in digits:
        return None

    ending = dic['ending']
    extended = ending in upper_alpha
    ending = ending.upper()
    if ending not in upper_alpha:
        return None

    if extended:
        prefix = 'Extended mode '
    else:
        prefix = 'Mode '

    return prefix + mode + ' ending on ' + ending + '.'

paren_display_funcs = {'6': paren_display_6
                       }

