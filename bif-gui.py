#!/usr/local/bin/python2

import cgi
import cgitb

cgitb.enable()

form = cgi.FieldStorage()

def tryInt(s, default):
    try:
        return int(s)
    except ValueError:
        return default
    except TypeError:
        return default

def tryFloat(s, default):
    try:
        return float(s)
    except ValueError:
        return default
    except TypeError:
        return default

width  = tryInt(form.getfirst("width"), 512)
height = tryInt(form.getfirst("height"), 512)
left   = tryFloat(form.getfirst("left"), -2)
bot    = tryFloat(form.getfirst("bot"), -0.5)
right  = tryFloat(form.getfirst("right"), 4)
top    = tryFloat(form.getfirst("top"), 1.5)
filt   = tryInt(form.getfirst("filt"), 1000)
disp   = tryInt(form.getfirst("disp"), 1000)
xzoom  = tryFloat(form.getfirst("xzoom"), 0.5)
yzoom  = tryFloat(form.getfirst("yzoom"), 0.5)

clickX = tryInt(form.getfirst("click.x"), None)
clickY = tryInt(form.getfirst("click.y"), None)

if (clickX != None) and (clickY != None):
    x = left + clickX * float(right - left) / width
    y = top  + clickY * float(bot   - top)  / height
    w, h = float(right - left) * xzoom / 2, float(top - bot) * yzoom / 2
    left, right = x - w, x + w
    bot , top   = y - h, y + h

print """Content-Type: text/html

<HTML><HEAD><TITLE>
Bifurcation Fractal CGI Inteface
</TITLE></HEAD><BODY BG=#ffffff>
<FORM action="bif-gui.py" METHOD=GET>
"""

print '<INPUT TYPE="image" name="click" alt=" [ Bifurcation Fractal ] "'
print 'src="bif.py?width=' + str(width) + '&height=' + str(height) \
+ '&left=' + str(left) + '&bot=' + str(bot) \
+ '&right=' + str(right) + '&top=' + str(top) \
+ '&filt=' + str(filt) + '&disp=' + str(disp) + '">'

print '<BR><BR>Click to zoom in.<BR>'
print '<BR>X Zoom factor (0.5 = 2x, 0.1 = 10x):'
print '<INPUT TYPE="text" name="xzoom" value="' + str(xzoom) + '">'
print '<BR>Y Zoom factor (0.5 = 2x, 0.1 = 10x):'
print '<INPUT TYPE="text" name="yzoom" value="' + str(yzoom) + '">'
print '<BR>Image width (in pixels):'
print '<INPUT TYPE="text" name="width"  value="' + str(width ) + '">'
print '<BR>Image height (in pixels):'
print '<INPUT TYPE="text" name="height" value="' + str(height) + '">'
print '<BR>Filter cycles (before displaying points):'
print '<INPUT TYPE="text" name="filt" value="' + str(filt) + '">'
print '<BR>Display cycles:'
print '<INPUT TYPE="text" name="disp" value="' + str(disp) + '">'
print '<BR>'
print '<BR>Reigion to display (for precise control; if you edit these values, click on "Regenerate" instead of the image or chaos will ensue):'
print '<BR>Left boundary:'
print '<INPUT TYPE="text" name="left" value="' + str(left) + '">'
print '<BR>Right boundary:'
print '<INPUT TYPE="text" name="right" value="' + str(right) + '">'
print '<BR>Bottom boundary:'
print '<INPUT TYPE="text" name="bot" value="' + str(bot) + '">'
print '<BR>Top boundary:'
print '<INPUT TYPE="text" name="top" value="' + str(top) + '">'
print '<BR>'
print "<BR>Don't press enter with the cursor in any of the above fields."
print '<BR>Instead, either click on the image to zoom in, or click:'
print '<BR><INPUT TYPE="submit" value="Regenerate">'
print '<BR>'
print '<BR>See also: <A HREF="mand-gui.py">Mandelbrot fractal</A> viewer.'
print '<BR>(<A HREF="mand.py?width=512&height=512&left=-2&right=2&bot=-2&top=2&iter=150">' \
      'Without GUI</A>)'

print
print '</FORM></BODY></HTML>'
