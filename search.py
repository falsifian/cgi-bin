"""search.py

TODO list:
- Allow output formats besides HTML (e.g. the original output format).
- Consider putting optional line numbers in the output.
- Stream output
- should a verse associated with e.g. an antiphon be searched if only
  antiphons are selected for searching?
"""

from pybol import *
from lfile import *
from format import *

def merge_intervals(ints):
    check = 0
    while check < len(ints) - 1:
        if ints[check][1] >= ints[check + 1][0]:
            ints[check : check + 2] = [(ints[check][0],
                                        max(ints[check][1],
                                            ints[check + 1][1]))]
        else:
            check += 1

def office_has_alt_pat(office, alt_pat):
    for info_block in office.contents:
        if info_block.section_name != 'info_block':
            return False # We've checked all info blocks in the office.

        if None != search(info_block.text, alt_pat, info_block.text_start,
                          info_block.text_end):
            return True

    return False # We've checked every section in the office.

class Restriction:
    def check_section(self, text, section):
        pass

class InfoRestriction(Restriction):
    def __init__(self, type_name, info_pat):
        self.type = type_name
        self.info_pat = info_pat

    def check_section(self, text, section):
        return section.type != self.type  or \
               None != match(section.info, self.info_pat)

class ContentRestriction(Restriction):
    def __init__(self, type_name, content_type, content_pat):
        self.type = type_name
        self.content_type = content_type
        self.content_pat = content_pat

    def check_section(self, text, section):
        if section.type != self.type:
            return True

        for section2 in section.contents:
            if section2.type == content_type:
                if None != search(text, self.content_pat, section2.start,
                                  section2.end):
                    return True

        return False

class SearchPat:
    def __init__(self, type_name, search_pat):
        self.type = type_name
        self.search_pat = search_pat

    def highlights(self, text, section):
        # Returned highlights are unsorted and unmerged.
        if section.type != self.type:
            return []

        regions = []
        for i in xrange(section.start, section.end):
            for state in self.search_pat(PatState(text, curs = i,
                                                  end = section.end)):
                regions.append((i, state.curs))

#        regions.append((section.start, section.end))

        return regions

class Query:
    def __init__(self, restrictions, search_pats):
        self.restrictions = restrictions
        self.search_pats = search_pats

    def check_section(self, text, section):
        for restriction in self.restrictions:
            if not restriction.check_section(text, section):
                return False

        return True

    def highlights(self, text, section):
        regions = []
        for search_pat in self.search_pats:
            regions.extend(search_pat.highlights(text, section))
        regions.sort()
        merge_intervals(regions)
        return regions

null_query = Query((), ())

class SearchResult:
    def __init__(self, text, parse_result, query = null_query,
                 keep_empty = False):
        self.text = text
        self.title = parse_result.title
        self.leaf = parse_result.leaf

        if self.leaf:
            self.start = parse_result.start
            self.end = parse_result.end
            self.highlights = query.highlights(text, parse_result)
            self.empty = len(self.highlights) == 0
        else:
            self.empty = not query.check_section(text, parse_result)

            if self.empty:
                if keep_empty:
                    self.contents = [SearchResult(text, section,
                                                  keep_empty = True)
                                     for section in parse_result.contents]
                else:
                    self.contents = []
            else:
                self.empty = True # Empty unless some subsection is not.
                self.contents = []
                for section in parse_result.contents:
                    search_res = SearchResult(text, section, query, keep_empty)
                    if not search_res.empty:
                        self.empty = False
                        self.contents.append(search_res)
                    elif keep_empty:
                        self.contents.append(search_res)

    def to_html(self, text, write, context = ()):
        if self.title != None:
            context = context + (self.title,)

        if self.leaf:
            write('<HR>\n')
            write('<I>[' + fmt_context(context) + ']</I><BR>\n')
            fmt_html(text, (self.start, self.end), self.highlights, write)
        else:
            for section in self.contents:
                section.to_html(text, write, context)

def search_file(text, parse_result, query = null_query, keep_empty = False):
    return SearchResult(text, parse_result, query, keep_empty)
