#!/usr/local/bin/python2

import math
import cgi
import cgitb

# If n is prime, divis(n) returns None.
# If n is not prime, divis(n) returns its smallest factor.
# divis is never passed 1 as a paramater.

def divis(n):
    d, max = 2, int(math.sqrt(n))
    while d <= max:
        if n % d == 0:
            return d
        if d % 10000 == 0:
            print 'Testing ', d, '... <BR>' # Print a status report.
        d = d + 1
    return None

cgitb.enable() # Prints a helpful message if the script has a runtime 
               # error.

form = cgi.FieldStorage()

print """Content-Type: text/html

<HTML><HEAD><TITLE>Prime Number Checker</TITLE></HEAD>
<BODY BG=#ffffff>
"""

n = form.getfirst("n")
if n == None:
    print 'Please specify a value for n.'
else:
    try:
        ni = int(n)
    except ValueError:
        print "Couldn't parse " + n + ' as an integer.'
    else:
        if ni == 1:
            print '1 is not prime.'
        else:
            pr = divis(ni)
            print ni,
            if pr == None:
                print ' is prime.'
            else:
                print ' is divisible by',
                print pr,
                print '.'

print """
</BODY></HTML>
"""
