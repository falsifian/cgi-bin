"""format.py
"""

from paren_display import *

leftgil  = '\xae'
rightgil = '\xaf'

def fmt_index(index, depth):
    if index == None:
        return ''
    else:
        return str(index) + '. '

_small_nums = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth',
               'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth',
               'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth',
               'seventeenth', 'eighteenth', 'nineteenth']

def fmt_Num(index):
    return _small_nums[index - 1][0].upper() + _small_nums[index - 1][1:]

html_space = '&nbsp;'

def fmt_html(text, text_range, highlights, write):
    curs = text_range[0]
    highlighting = False
    boldface = False
    paren_display_func = None
    
    while curs < text_range[1]:
        if highlighting:
            if curs >= highlight_end:
                write('</B>')
                if boldface:
                    write('<B>')
                highlighting = False
        elif len(highlights) != 0 and curs >= highlights[0][0]:
            if boldface:
                write('</B>')
            write('<B style="background-color:#80FFA0">')
            highlighting = True
            highlight_end = highlights[0][1]
            highlights = highlights[1:]

        if text.startswith(leftgil + 'MDBO' + rightgil, curs):
            if not (highlighting or boldface):
                write('<B>')
            boldface = True
            curs += 5
        elif text.startswith(leftgil + 'MDNM' + rightgil, curs):
            if boldface and not highlighting:
                write('</B>')
            boldface = False
            curs += 5
        elif text[curs] == '\n':
            write('<BR>\n')
        elif text[curs] == '\t':
            write(html_space * 8)
        elif text[curs] != '\r':
            write(text[curs])
            
            if text[curs] == '(' and curs + 1 < text_range[1]:
                paren_display_func = paren_display_funcs.get(text[curs + 1])
                paren_start = curs
            elif text[curs] == ')' and paren_display_func != None:
                info = paren_display_func(text, paren_start, curs + 1)
                if info != None: write(' <I>[' + info + ']</I> ')
                paren_display_func = None

        curs += 1

    if highlighting or boldface:
        write('</B>')

def fmt_context(context):
    if len(context) == 0:
        return ''

    s = context[0]
    for t in context[1:]:
        s += ' / ' + t

    return s
