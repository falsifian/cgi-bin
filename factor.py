#!/usr/bin/env python

import random
import cgi
import cgitb

cgitb.enable()

def add_repr(n):
    if n >= 0:
        s = '+ '
    else:
        s = '- '
        n = -n
    return s + `n`

print """Content-Type: text/html

<HTML><HEAD><TITLE>Factor</TITLE></HEAD>
<BODY BG=#ffffff>
"""

form = cgi.FieldStorage()

s = form.getfirst('s')
p = form.getfirst('p')
a = form.getfirst('a')
b = form.getfirst('b')
if s != None and p != None and a != None and b != None:
    a = int(a)
    b = int(b)
    s = int(s)
    p = int(p)
    print 'x^2 + ' + `s` + 'x + ' + `p` + '<BR>'
    print '= (x ' + add_repr(a) + ')(x ' + add_repr(b) + ')<BR>'
    print '<BR>'
    if a * b == p and a + b == s:
        print 'CORRECT<BR>'
        p = s = None
    else:
        print 'INCORRECT<BR>'

if p == None or s == None:
    random.seed()
    a = 0
    while a == 0:
        a = random.randrange(-10, 11)
    b = 0
    while b == 0:
        b = random.randrange(-10, 11)
    
    p = a * b
    s = a + b

print """
<FORM ACTION="factor.py" METHOD=GET>
"""

print 'x^2 ' + add_repr(s) + ' ' + add_repr(p) + '<BR>'
print '(x + <INPUT TYPE="text" name="a">)(x + <INPUT TYPE="text" name="b">)<BR>'
print '<INPUT TYPE="hidden" name="s" value="' + `s` + '">'
print '<INPUT TYPE="hidden" name="p" value="' + `p` + '">'
print '<INPUT TYPE="submit">'
print '</FORM></BODY></HTML>'
