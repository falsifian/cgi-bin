#!/usr/local/bin/python2

import cgitb

cgitb.enable()

import os
import cgi

def htmlError(errStr):
    print 'Content-Type: text/html'
    print
    print '<HTML><HEAD><TITLE>Image Generator: ' + errStr
    print '</TITLE></HEAD><BODY>'
    print errStr
    print '</BODY></HTML>'

form = cgi.FieldStorage()

widthStr = form.getfirst("width")
heightStr = form.getfirst("height")
leftStr = form.getfirst("left")
topStr = form.getfirst("top")
rightStr = form.getfirst("right")
botStr = form.getfirst("bot")
iterStr = form.getfirst("iter")

if not (widthStr and heightStr and leftStr and topStr
     and rightStr and botStr and iterStr):
    htmlError('Please specify values for "width", "height", ' +
              '"left", "top", "right", "bot" and "iter".')
else:
    try:
        width  = int(widthStr)
        height = int(heightStr)
        left = float(leftStr)
        top = float(topStr)
        right = float(rightStr)
        bot = float(botStr)
        iter = int(iterStr)
    except ValueError, instance:
        htmlError('Error parsing parameters: ' + `instance`)
    else:
        print 'Content-Type: image/png'
        print
        import sys
        sys.stdout.flush()
	os.nice(20)
        os.execl('/cs/www4/htuser/jcook/bin/mand', 'mand', str(width), str(height),
                 str(left), str(bot), str(right), str(top), str(iter))
