#!/usr/local/bin/python2

import cgi
import cgitb

cgitb.enable()

form = cgi.FieldStorage()

def tryInt(s, default):
    try:
        return int(s)
    except ValueError:
        return default
    except TypeError:
        return default

def tryFloat(s, default):
    try:
        return float(s)
    except ValueError:
        return default
    except TypeError:
        return default

width  = tryInt(form.getfirst("width"), 512)
height = tryInt(form.getfirst("height"), 512)
left   = tryFloat(form.getfirst("left"), -2)
bot    = tryFloat(form.getfirst("bot"), -2)
right  = tryFloat(form.getfirst("right"), 2)
top    = tryFloat(form.getfirst("top"), 2)
iter   = tryInt(form.getfirst("iter"), 150)
zoom   = tryFloat(form.getfirst("zoom"), 0.2)

clickX = tryInt(form.getfirst("click.x"), None)
clickY = tryInt(form.getfirst("click.y"), None)

if (clickX != None) and (clickY != None):
    x = left + clickX * (right - left) / width
    y = top  + clickY * (bot   - top)  / height
    w, h = (right - left) * zoom / 2, (top - bot) * zoom / 2
    left, right = x - w, x + w
    bot , top   = y - h, y + h

print """Content-Type: text/html

<HTML><HEAD><TITLE>
Mandelbrot CGI Interface
</TITLE></HEAD><BODY BG=#ffffff>
<FORM action="mand-gui.py" METHOD=GET>
"""

print '<INPUT TYPE="hidden" name="left"   value="' + str(left  ) + '">'
print '<INPUT TYPE="hidden" name="bot"    value="' + str(bot   ) + '">'
print '<INPUT TYPE="hidden" name="right"  value="' + str(right ) + '">'
print '<INPUT TYPE="hidden" name="top"    value="' + str(top   ) + '">'

print '<INPUT TYPE="image" name="click" alt=" [ Mandelbrot Fractal ] "'
print 'src="mand.py?width=' + str(width) + '&height=' + str(height) \
+ '&left=' + str(left) + '&bot=' + str(bot) \
+ '&right=' + str(right) + '&top=' + str(top) \
+ '&iter=' + str(iter) + '">'

print '<BR>Zoom factor (0.5 = 2x, 0.1 = 10x):'
print '<INPUT TYPE="text" name="zoom" value="' + str(zoom) + '">'
print '<BR>Max iterations:'
print '<INPUT TYPE="text" name="iter" value="' + str(iter) + '">'
print '<BR>Image width (in pixels):'
print '<INPUT TYPE="text" name="width"  value="' + str(width ) + '">'
print '<BR>Image height (in pixels):'
print '<INPUT TYPE="text" name="height" value="' + str(height) + '">'
print '<BR><INPUT TYPE="submit" value="Regenerate">'

print
print '</FORM></BODY></HTML>'
