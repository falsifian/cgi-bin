#!/usr/local/bin/python2

import cgi
import os
import cgitb

cgitb.enable()

def htmlError(errStr):
    print 'Content-Type: text/html'
    print
    print '<HTML><HEAD><TITLE>Image Generator: ' + errStr
    print '</TITLE></HEAD><BODY>'
    print errStr
    print '</BODY></HTML>'

form = cgi.FieldStorage()

widthStr = form.getfirst("width")
heightStr = form.getfirst("height")
leftStr = form.getfirst("left")
topStr = form.getfirst("top")
rightStr = form.getfirst("right")
botStr = form.getfirst("bot")
filtStr = form.getfirst("filt")
dispStr = form.getfirst("disp")

if not (widthStr and heightStr and leftStr and topStr
     and rightStr and botStr and filtStr and dispStr):
    htmlError('New parameters: filt and disp.\n'
              'Please specify values for "width", "height", ' +
              '"left", "top", "right", "bot", "filt" and "disp".\n'
              '"filt" controls the number of times to filter '
              'a number before displaying it, and "disp" controls the '
              'number of points to draw after filtering.')
else:
    try:
        width  = int(widthStr)
        height = int(heightStr)
        left = float(leftStr)
        top = float(topStr)
        right = float(rightStr)
        bot = float(botStr)
        filt = int(filtStr)
        disp = int(dispStr)
    except ValueError, instance:
        htmlError('Error parsing parameters: ' + instance)
    else:
        print 'Content-Type: image/png'
        print
	os.nice(20)
        os.execl('/cs/www4/htuser/jcook/bin/bif', 'mand', str(width), str(height),
                 str(left), str(bot), str(right), str(top), str(filt),
                 str(disp))

