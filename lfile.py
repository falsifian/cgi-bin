### lfile.py

from pybol import *
from format import *

"""
File structure

A section is a dictionary with the following entries:
- 'type': what sort of entry it is:
   file, office, info, service, line, other
- 'info': optional additional information; exact meaning depends on 'type'.
- 'title': A title identifying the section; not in the dictionary if the
  section should not have a title.
- 'contents': a list of other dictionary entries where lists of
  entries may be found.
- 'start', 'end': if applicable, the range of text to be searched or
  displayed
- 'leaf': None if this section has no subsections, not in dictionary
  otherwise.
"""

genre_titles = {'A': 'Antiphon',
                'R': 'Responsory',
                'V': 'Verse',
                'D': 'Dialogue',
                'H': 'Hymn',
                'Q': 'Sequence',
                'I': 'Invitatory',
                'E': 'Gospel Antiphon',
                'X': 'Doxology'}

service_titles = {'M': 'Matins',
                  'V': 'Vespers',
                  'L': 'Lauds',
                  'W': 'Second Vespers',
                  'P': 'Prime',
                  'T': 'Tense',
                  'S': 'Sext',
                  'N': 'None',
                # 'R': ???,
                  '$': 'Suffrage'}

### Simple patterns

term = leftgil + rightgil + ' \t\n'
sec_rem = cat(rep(not_any('|')), exact('\n'))
sec_start = break_at('|\n')
LLNN = cat(any(upper_alpha), any(upper_alpha), any(digits), any(digits))
file_top = cat(break_at('|'), exact('|'), sec_rem)

### Big patterns

catch_all = cat(getpos('start'), arb, getpos('end'),
                label('type', 'other'),
                label('title', 'Catch All'),
                label('leaf', None))

# Individual Line

info_block = cat(getpos('start'), sec_start, exact('|g'), sec_rem,
                 getpos('end'),
                 label('type', 'info'),
                 label('title', 'General Information Section'),
                 label('leaf', None))

line = cat(getpos('start'), sec_start, exact('|g'),
           span(digits, allow_empty = True), break_at('='), skip(2),
           set('info', skip(1)), sec_rem, getpos('end'),
           label('type', 'line'),
           label('leaf', None))

titled_line = cat(line,
                  applyP(lambda i:
                         label('title',
                               genre_titles.get(i, 'Unknown Genre') +
                               ' (' + i + ')'),
                         'info'))

verse_or_doxology = cat(titled_line,
                        test(lambda i: i in 'VD', # make sure it's a verse or
                                                  # doxology
                             'info' # pass the above function the line type
                             ))

# Block of Lines

line_block = cat(repdic(line, 'head', min = 1, max = 1),
                 repdic(verse_or_doxology, 'verses'),
                 label('type', 'other'),
                 applyP(lambda h:
                        label('info', h[0]['info']),
                        'head'),
                 applyP(lambda i:
                        label('title',
                              genre_titles.get(i, 'Unknown Genre') +
                              ' (' + i + ')'),
                        'info'),
                 label('contents', ('head', 'verses')))

def line_block_of_kind(kinds):
    return cat(line_block, test(lambda head: head[0]['info'] in kinds,
                                'head'))

# Service

service_head = cat(sec_start, exact('|.'), LLNN, exact('='),
                   set('info', skip(1)), sec_rem,
                   applyP(lambda i:
                          label('title',
                                service_titles.get(i, 'Unknown Service') +
                                ' (' + i + ')'),
                          'info'))

nocturn = cat(repdic(line_block_of_kind('A'), 'antiphons', min = 1),
              repdic(line_block_of_kind('R'), 'responsories', min = 1),
              label('type', 'other'),
              label('title', 'Nocturn'),
              label('contents', ('antiphons', 'responsories')))

service = cat(service_head,
              alt(cat(test(lambda s: s == 'M', # Is this a matins service?
                           'info'),
                      repdic(line_block_of_kind('I'), 'invitatory', max = 1),
                      repdic(nocturn, 'nocturns'),
                      label('type', 'service'),
                      label('contents', ('invitatory', 'nocturns'))),
                  cat(repdic(line_block, 'lines'),
                      label('type', 'service'),
                      label('contents', ('lines',)))))

# Office

office = cat(sec_start, exact('|.'), set('info', LLNN), any(term), sec_rem,
             repdic(info_block, 'general_information'),
             repdic(service, 'services'),
             label('type', 'office'),
             applyP(lambda i:
                    label('title', 'Office ' + i),
                    'info'),
             label('contents', ('general_information', 'services')))

# File

# file_pat expects 'info' to be set to the file name
file_pat = cat(file_top,
               repdic(office, 'offices'),
               label('type', 'file'),
               applyP(lambda i:
                      label('title', 'File ' + i),
                      'info'),
               label('contents', ('offices',)))
               

### ParseResult: a class to store the results of a parse.

def add_indicies(contents):
    infos_encountered = {}
    for i in xrange(len(contents)):
        info = contents[i].info
        k = infos_encountered.get(info)
        if k == None:
            infos_encountered[info] = [False, i]
        elif k[0]:
            k[1] += 1
            if contents[i].title != None:
                contents[i].title = fmt_Num(k[1]) + ' ' \
                                    + contents[i].title
        else:
            if contents[k[1]].title != None:
                contents[k[1]].title = fmt_Num(1) + ' ' \
                                       + contents[k[1]].title
            if contents[i].title != None:
                contents[i].title = fmt_Num(2) + ' ' \
                                    + contents[i].title
            k[0] = True
            k[1] = 2

class ParseResult:
    """
    type, info, title, start, end -- see File Structure, at the top of this
                                     file
    contents -- a list of ParseResults
    """
    def __init__(self, text, dic):
        self.type = dic['type']
        self.info = dic.get('info')
        self.title = dic.get('title')
        if dic.has_key('start'):
            self.start = dic['start']
            self.end   = dic['end']
        self.leaf = dic.has_key('leaf')

        if dic.has_key('contents'):
            self.contents = [ParseResult(text, section)
                             for name in dic['contents']
                             for section in dic[name]]
            add_indicies(self.contents)

def parse_file(text, file_name):
    dic = match(text, file_pat, init_dic = {'info': file_name})
    if dic == None:
        return None
    else:
        return ParseResult(text, dic)
